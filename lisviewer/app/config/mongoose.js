var config = require('./config');
var mongoose = require('mongoose');

module.exports = new function () {
    this.db;
    this.connect = function () {
        mongoose.set('debug', config.debug);
        console.log('Connecting to mongodb via mongoose.');
        this.db = mongoose.connect(config.mongoUri);
        // Catching the events
        mongoose.connection.on('connected', function () {
            console.log('Mongoose connected to ' + config.mongoUri);
        });
        mongoose.connection.on('error', function (err) {
            console.log('Mongoose connection error: ' + err);
        });
        mongoose.connection.on('disconnected', function () {
            console.log('Mongoose disconnected');
        });
        process.on('SIGINT', function () {
            mongoose.connection.close(function () {
                console.log('Mongoose disconnected through app termination');
                process.exit(0);
            });
        });

        // load model
//        require('../app/models/counter.server.model');
        return this.db;
    };

    this.disconnect = function () {
        mongoose.connection.close(function () {
            console.log('Mongoose default connection closed');
            process.exit(0);
        });
    };
};