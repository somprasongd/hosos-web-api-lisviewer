var express = require('express');
var router = express.Router();

// load config
var config = require('../config/config');

var fl = require('../util/filelist');

router.get('/result/:ln', function (req, res, next) {
    var files = [];
    fl.walkSync(config.lisResultUri + req.param('ln'), function (file) {
        if (file !== undefined) {
            files.push(file);
        }
    });
    res.json(files);
});

module.exports = router;
