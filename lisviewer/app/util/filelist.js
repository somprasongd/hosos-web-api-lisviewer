var fs = require('fs');
var path = require('path');
var FileList = function () {

    var getFileObj = function (name, path, stat) {
        var file = {
            name: name,
            fileUri: path,
            stat: {
                size: stat.size,
                atime: stat.atime,
                mtime: stat.mtime,
                ctime: stat.ctime,
                birthtime: stat.birthtime
            }};
        return file;
    };
    // sync version
    this.walkSync = function (currentDirPath, callback) {
        try {
            if (fs.lstatSync(currentDirPath).isDirectory()) {
                fs.readdirSync(currentDirPath).forEach(function (name) {
                    var filePath = path.join(currentDirPath, name);
                    var stat = fs.statSync(filePath);
                    if (stat.isFile()) {
                        callback(getFileObj(name, filePath, stat));
                    } else if (stat.isDirectory()) {
                        walkSync(filePath, callback);
                    }
                });
            }
        } catch (e) {
            callback(undefined);
        }
    };
    // async version with basic error handling
    this.walk = function (currentDirPath, callback) {
        fs.readdir(currentDirPath, function (err, files) {
            if (err) {
//                throw new Error(err);
                callback(undefined);
            }
            files.forEach(function (name) {
                var filePath = path.join(currentDirPath, name);
                var stat = fs.statSync(filePath);
                if (stat.isFile()) {
                    callback(getFileObj(name, filePath, stat));
                } else if (stat.isDirectory()) {
                    walk(filePath, callback);
                }
            });
        });
    };
};
// sync version
module.exports = new FileList();