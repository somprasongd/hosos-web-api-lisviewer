#Docker requires a 64-bit installation regardless of your Ubuntu version. Additionally, your kernel must be 3.10 at minimum.
#To check your current kernel version, open a terminal and use uname -r to display your kernel version:

echo "Update apt sources"

# 1. Update package information, ensure that APT works with the https method, and that CA certificates are installed.
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates

# 2. the new GPG key.
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

# 3. Repository
sudo echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" > /etc/apt/sources.list.d/docker.list

# 4. Update the APT package index.
sudo apt-get update

# 5. Install the recommended packages.
sudo apt-get install linux-image-generic-lts-trusty

# 6. Reboot your host.
sudo reboot