echo "Install Docker Engine"

# 1. Update your APT package index.
sudo apt-get update

# 2. Install Docker.
sudo apt-get install docker-engine

# 3. Start the docker daemon.
sudo service docker start 