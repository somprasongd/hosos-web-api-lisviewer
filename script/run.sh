cd ..

echo "Install NPM"
cd lisviewer
npm install

echo "docker-compose build"
cd ..
docker-compose build

echo "docker-compose up"
docker-compose up -d
